use clap;
use csv;
use anyhow::Result;

#[tokio::main]
async fn main() -> Result<()> {
    let matches = clap::App::new("CLI Tool").version("0.1.0").author("Yuanzhi Lou").about("Command-line tool for Rust")
        .arg(clap::Arg::with_name("file").help("Input CSV file").required(true).index(1))
        .arg(clap::Arg::with_name("low").help("Lower bound of Height").short("l").long("low").takes_value(true).required(true))
        .arg(clap::Arg::with_name("high").help("Upper bound of Height").short("h").long("high").takes_value(true).required(true))
        .get_matches();
    let path = matches.value_of("file").unwrap();
    let low = matches.value_of("low").unwrap();
    let high = matches.value_of("high").unwrap();
    let mut reader = csv::Reader::from_path(path)?;
    for result in reader.records() {
        let record = result?;
        if record[1].parse::<f32>()? >= low.parse::<f32>()? && record[1].parse::<f32>()? <= high.parse::<f32>()? {
            println!("{:?}", record);
        }
    }
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    #[tokio::test]
    async fn test1() -> Result<()> {
        let path = "data.csv";
        let low = "170";
        let high = "180";
        let mut reader = csv::Reader::from_path(path)?;
        for result in reader.records() {
            let record = result?;
            if record[1].parse::<f32>()? >= low.parse::<f32>()? && record[1].parse::<f32>()? <= high.parse::<f32>()? {
                assert_eq!(record[0].to_string(), "Qian");
                assert_eq!(record[1].parse::<f32>()?, 173.2);
            }
        }
        Ok(())
    }
}



