# Yl954 Mini Project 8
This project uses ```clap``` to create a command-line tool for csv processing. The tool will ingest the data in the csv file, filter and print the processed data based on the the arguments we give on the command line.

## Author
Yuanzhi Lou

## Dependencies
```
qdrant-client = "1.8.0"
csv = "1.1.6"
clap = "2.27.1"
tokio = { version = "1", features = ["rt-multi-thread"] }
anyhow = "1.0.0"
```

## data.csv
```
Name,height
Chen,192.6
Li,187.4
Song,168.5
Qian,173.2
Tang,150.0
Zhao,140.2
Zhou,90.8
Wu,200.1
Zheng,190.9
Wang,189.0
```

## Tool Functionality
This tool accepts three arguments: file, low, and high, which will print all the records within the height range.
- file: Path of the CSV file
- low: Lower bound of Height
- high: Upper bound of Height

### Usage
1. ```cargo run <file> -h <high> -l <low>```
2. ```cargo run <file> --high <high> --low <low>```

### Result
```cargo run data.csv -h 190 -l 150```
![](image.png)

## Data Processing
The data read from data.csv is filtered based on the given height range.
```
let path = matches.value_of("file").unwrap();
    let low = matches.value_of("low").unwrap();
    let high = matches.value_of("high").unwrap();
    let mut reader = csv::Reader::from_path(path)?;
    for result in reader.records() {
        let record = result?;
        if record[1].parse::<f32>()? >= low.parse::<f32>()? && record[1].parse::<f32>()? <= high.parse::<f32>()? {
            println!("{:?}", record);
        }
    }
```

## Testing Implementation

```
#[cfg(test)]
mod tests {
    use super::*;
    #[tokio::test]
    async fn test1() -> Result<()> {
        let path = "data.csv";
        let low = "170";
        let high = "180";
        let mut reader = csv::Reader::from_path(path)?;
        for result in reader.records() {
            let record = result?;
            if record[1].parse::<f32>()? >= low.parse::<f32>()? && record[1].parse::<f32>()? <= high.parse::<f32>()? {
                assert_eq!(record[0].to_string(), "Qian");
                assert_eq!(record[1].parse::<f32>()?, 173.2);
            }
        }
        Ok(())
    }
}
```
The testing results are shown below by using ```cargo test```.
![](image-1.png)